# wine in ubuntu versions less than 17.04 hangs in the tests
FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    cmake \
    gcc \
    libc6-dev \
    make \
    pkg-config \
    zip \
    wget
    
RUN apt-get install -y p7zip

RUN dpkg --add-architecture i386 && apt-get update && \
    apt-get install -y --no-install-recommends \
        wine-stable \
        wine64 \
        wine32 \
        libz-mingw-w64-dev

RUN apt-get install -y --no-install-recommends g++-mingw-w64-x86-64

# install rustup
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

# rustup directory
ENV PATH /root/.cargo/bin:$PATH

# add win64 target
RUN rustup target add x86_64-pc-windows-gnu

# run-detectors are responsible for calling the correct interpreter for exe
# files. For some reason it does not work inside a docker container (it works
# fine in the host). So we replace the usual paths of run-detectors to run wine
# directly. This only affects the guest, we are not messing up with the host.
#
# See /usr/share/doc/binfmt-support/detectors
RUN mkdir -p /usr/lib/binfmt-support/ && \
    rm -f /usr/lib/binfmt-support/run-detectors /usr/bin/run-detectors && \
    ln -s /usr/bin/wine /usr/lib/binfmt-support/run-detectors && \
    ln -s /usr/bin/wine /usr/bin/run-detectors

# cleanup
RUN apt autoremove -y
RUN apt clean -y
RUN rm -rf /tmp/* /var/tmp/*

# Add openssl for windows
RUN cd && \
    wget https://www.npcglib.org/~stathis/downloads/openssl-1.1.0f-vs2017.7z && \
    7zr x openssl-1.1.0f-vs2017.7z && \
    mv openssl-1.1.0f-vs2017 /root/openssl

ENV CARGO_TARGET_X86_64_PC_WINDOWS_GNU_LINKER=x86_64-w64-mingw32-gcc \
    CARGO_TARGET_X86_64_PC_WINDOWS_GNU_RUNNER=wine \
    CC_x86_64_pc_windows_gnu=x86_64-w64-mingw32-gcc \
    CXX_x86_64_pc_windows_gnu=x86_64-w64-mingw32-g++ \
    RUST_BACKTRACE=1
